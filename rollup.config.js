const liveServer = require('rollup-plugin-live-server');
const path = require('path');
const root = process.cwd();

const entry = path.resolve(root, 'src', 'index.mjs');
const mode = process.env.NODE_ENV;
const dev = mode === 'development';

export default [
	{
    input: entry,
    plugins: [
      dev && liveServer({
        port: 8001,
        host: '0.0.0.0',
        file: 'mocha.html',
        mount: [['/tests', './tests'], [ '/src', './src' ], [ '/node_modules', './node_modules' ]],
        open: false,
        wait: 500
      })
    ],
    output: {
      file: path.resolve(root, 'dist', 'tea.mjs'),
      format: 'es'
    }
  },
	{
    input: entry,
    output: {
      file: path.resolve(root, 'index.js'),
      format: 'cjs'
    }
  }
];
